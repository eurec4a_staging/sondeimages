import os

import jinja2
import numpy as np
import matplotlib.pylab as plt
import cartopy.crs as ccrs
import eurec4a
import ipfsspec  # noqa: F401
from tiles import specMACSPTiles
import tqdm
import xarray as xr


class TemplatePage:
    def __init__(self, template_name, title, short_title=None, href=None):
        self.template_name = template_name
        self.title = title
        self.short_title = short_title or title
        self._href = href

    @property
    def slug(self):
        return self.short_title.replace(" ", "_").lower()

    @property
    def href(self):
        return self._href or f"{self.slug}.html"

    def render(self, env, sounding_info_by_parameter, parameters):
        tpl = env.get_template(self.template_name)
        return tpl.render(slug=self.slug,
                          title=self.title,
                          pages=PAGES)


class ParameterPage:
    def __init__(self, order_key, parameter, template_name="sondeimages.html"):
        self.order_key = order_key
        self.parameter = parameter
        self.template_name = template_name

    @property
    def title(self):
        return f"{self.parameter} by {self.order_key}"

    @property
    def short_title(self):
        return self.title

    @property
    def slug(self):
        return f"{self.parameter}_by_{self.order_key}"

    @property
    def href(self):
        return f"{self.slug}.html"

    def render(self, env, sounding_info_by_parameter, parameters):
        tpl = env.get_template(self.template_name)
        sounding_info = sounding_info_by_parameter[self.parameter]
        sorted_soundings = list(
                sorted(sounding_info,
                       key=lambda sounding: sounding[self.order_key]))
        return tpl.render(order_key=self.order_key,
                          parameter=self.parameter,
                          soundings=sorted_soundings,
                          parameter_config=parameters[self.parameter],
                          slug=self.slug,
                          title=self.title,
                          pages=PAGES)


MAP_SIZE = .05

PAGES = [
    TemplatePage("index.html", "Info", href="index.html"),
    ParameterPage("time", "rh"),
    # ParameterPage("PW", "rh"),
    ParameterPage("rh90", "rh"),
    ParameterPage("rh95", "rh"),
    ParameterPage("time", "cloud_flag"),
    ParameterPage("rh90", "cloud_flag"),
    ParameterPage("rh95", "cloud_flag"),
]

PARAMETERS = {
    "rh": {"cmap": "turbo", "vmin": 0, "vmax": 1, "title": "rH"},
    "cloud_flag": {"cmap": "cividis",
                   "vmin": 0, "vmax": 1,
                   "title": "Cloud Flag"},
}


def get_jinja2_env():
    base_path = os.path.abspath(os.path.dirname(__file__))
    template_path = os.path.join(base_path, "templates")
    loader = jinja2.FileSystemLoader(template_path)
    return jinja2.Environment(
        loader=loader,
        autoescape=jinja2.select_autoescape(['html', 'xml'])
    )


def filter_sondes(ds, flight_segments):
    segment_of_sonde = {
            sonde_id: segment["segment_id"]
            for platform_id, flights_of_platform in flight_segments.items()
            for flight_id, flight in flights_of_platform.items()
            for segment in flight["segments"]
            for sonde_flag, flagged_sondes in segment["dropsondes"].items()
            for sonde_id in flagged_sondes
            if "circling" not in segment["kinds"]}

    flight_of_sonde = {
            sonde_id: flight_id
            for platform_id, flights_of_platform in flight_segments.items()
            for flight_id, flight in flights_of_platform.items()
            for segment in flight["segments"]
            for sonde_flag, flagged_sondes in segment["dropsondes"].items()
            for sonde_id in flagged_sondes
            if "circling" not in segment["kinds"]}
    bad_flights = [
        "HALO-0119",  # no specmacs images available yet
        "HALO-0124",  # no specmacs images available yet
    ]
    ds = ds.isel(sonde_id=ds.platform.values == "HALO")
    in_a_segment = np.array([sonde_id in segment_of_sonde
                             for sonde_id in ds.sonde_id.values])
    ds = ds.isel(sonde_id=in_a_segment)
    ds["segment_id"] = (("sonde_id",), [segment_of_sonde[sonde_id]
                                        for sonde_id in ds.sonde_id.values])
    ds["flight_id"] = (("sonde_id",), [flight_of_sonde[sonde_id]
                                       for sonde_id in ds.sonde_id.values])
    for bad_flight_id in bad_flights:
        ds = ds.isel(sonde_id=ds.flight_id != bad_flight_id)
    return ds


def infogen(parameter, ds):
    for i in range(len(ds.sonde_id)):
        sounding_ds = ds.isel(sonde_id=i)
        sonde_id = str(sounding_ds.sonde_id.values)
        segment_id = str(sounding_ds.segment_id.values)

        yield {
            "href": f"images/sounding_{sonde_id}_{parameter}.png",
            "time": str(sounding_ds.launch_time.values),
            "id": sonde_id,
            "segment_id": segment_id,
            # "PW": float(sounding_ds.PW.values),
            "rh90": float(sounding_ds.rh90.values),
            "rh95": float(sounding_ds.rh95.values),
        }


def show_colorbar(cmap, vmin, vmax, title, **_):
    fig, ax = plt.subplots(figsize=(10, 1))
    vals = np.linspace(0, 1, 256)[np.newaxis, :]
    ax.imshow(vals, extent=[vmin, vmax, 0, 1], cmap=cmap, aspect="auto")
    ax.set(yticklabels=[])
    ax.tick_params(left=False)
    ax.set_title(title)
    return fig


def plot_sounding(sounding_ds, parameter_name, parameter_config):
    fig = plt.figure(frameon=False, figsize=(5, 5))
    ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree())
    center = (np.nanmean(sounding_ds.lat.values),
              np.nanmean(sounding_ds.lon.values))
    segment_id = str(sounding_ds.segment_id.values)
    specmacs = specMACSPTiles(segment_id)

    ax.set_extent([center[1]-MAP_SIZE,
                   center[1]+MAP_SIZE,
                   center[0]-MAP_SIZE,
                   center[0]+MAP_SIZE], ccrs.PlateCarree())

    ax.add_image(specmacs, 11)
    ax.scatter(sounding_ds.lon, sounding_ds.lat,
               c=sounding_ds[parameter_name],
               s=1,
               vmin=parameter_config["vmin"],
               vmax=parameter_config["vmax"],
               cmap=parameter_config["cmap"],
               transform=ccrs.PlateCarree())
    ax.axis("off")
    return fig


def get_joanne_l3():
    """get joanne L3 data includig cloudflag information"""

    rootfolder = "ipfs://QmYAU2PBR8mGJmRs3SBK7kLSnon1VXgLikSzUEeyJQuJQv"

    joanne_l3 = xr.open_zarr(rootfolder + "/level3",
                             consolidated=True)
    joanne_cloudflag = xr.open_zarr(rootfolder + "/cloudflag",
                                    consolidated=True)

    joanne_l3 = joanne_l3.swap_dims({"sounding": "sonde_id"})
    joanne_l3 = xr.merge([joanne_l3, joanne_cloudflag])

    return joanne_l3


def _main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--max_sondes", default=None, type=int,
                        help="limit number of processed sondes to this number")
    parser.add_argument("-s", "--skip_images",
                        default=False, action="store_true",
                        help="skip creation of images")
    parser.add_argument("-o", "--output_folder", default="out",
                        help="output folder for generated html")
    args = parser.parse_args()

    env = get_jinja2_env()

    flight_segments = eurec4a.get_flight_segments()

    images_folder = os.path.join(args.output_folder, "images")
    os.makedirs(images_folder, exist_ok=True)

    parameters = {name: {**config, "cbar_href": f"images/cbar_{name}.png"}
                  for name, config in PARAMETERS.items()}

    if not args.skip_images:
        for parameter, config in parameters.items():
            fig = show_colorbar(**config)
            fig.savefig(os.path.join(args.output_folder, config["cbar_href"]),
                        bbox_inches="tight")
            plt.close(fig)

    joanne_l3 = get_joanne_l3()
    ds = filter_sondes(joanne_l3,
                       flight_segments)
    ds["rh95"] = (ds.rh > .95).mean(dim="alt")
    ds["rh90"] = (ds.rh > .9).mean(dim="alt")

    nsondes = len(ds.sonde_id)
    if args.max_sondes is not None and args.max_sondes < nsondes:
        nsondes = args.max_sondes
        ds = ds.isel(sonde_id=slice(0, nsondes))

    sounding_info_by_parameter = {}
    for parameter, config in parameters.items():
        sounding_info = list(infogen(parameter, ds))
        sounding_info_by_parameter[parameter] = sounding_info
        if not args.skip_images:
            for i in tqdm.trange(nsondes):
                fig = plot_sounding(ds.isel(sonde_id=i), parameter, config)
                fig.savefig(os.path.join(args.output_folder,
                                         sounding_info[i]["href"]))
                plt.close(fig)

    for page in PAGES:
        out = page.render(env, sounding_info_by_parameter, parameters)
        out_path = os.path.join(args.output_folder, page.href)
        with open(out_path, "w") as outfile:
            outfile.write(out)


if __name__ == "__main__":
    _main()
