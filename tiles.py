from cartopy.io.img_tiles import GoogleWTS
import numpy as np
from PIL import Image
import six


class DefaultTileWTS(GoogleWTS):
    def get_image(self, tile):
        if six.PY3:
            from urllib.request import urlopen, Request, HTTPError, URLError
        else:
            from urllib2 import urlopen, Request, HTTPError, URLError

        url = self._image_url(tile)
        try:
            request = Request(url)
            fh = urlopen(request)
            im_data = six.BytesIO(fh.read())
            fh.close()
            img = Image.open(im_data)

        except (HTTPError, URLError):
            img = self._default_tile(tile)

        img = img.convert(self.desired_tile_form)
        return img, self.tileextent(tile), 'lower'


class specMACSPTiles(DefaultTileWTS):
    def __init__(self, flight_segment_id):
        super(specMACSPTiles, self).__init__(desired_tile_form="RGBA")
        self.flight_segment_id = flight_segment_id
        self.empty_tile = Image.fromarray(np.full((256, 256, 4),
                                                  (255, 255, 255, 0),
                                                  dtype=np.uint8))

    def _default_tile(self, tile):
        return self.empty_tile

    def _image_url(self, tile):
        x, y, z = tile
        return (f"https://macsserver.physik.uni-muenchen.de/campaigns/EUREC4A/"
                f"maps/tiles/{self.flight_segment_id}/{z}/{x}/{y}.png")
